import players.Gladiator;
import players.Tiger;

public class MainApp {

    public static void main(String[] args) {
        Gladiator gladiator = new Gladiator("Russel", 10);

        Tiger tiger = new Tiger("tom", 5);
        System.out.println(tiger.getName());
        System.out.println(tiger.getHealthPoints());
        int i = 0;
        while( i<6){
            System.out.println("Dice result: " + tiger.rollDice());
            i++;
        }



    }
}
