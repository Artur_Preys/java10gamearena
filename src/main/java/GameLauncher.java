import arena.BattleArena;
import players.Archer;
import players.Gladiator;
import players.SuperHero;
import players.Tiger;

public class GameLauncher {

    public static void main(String[] args) {
        System.out.println("Hello test");
        BattleArena battleArena = new BattleArena();
        Gladiator gladiator = new Gladiator("Russel", 5);
        Tiger tiger= new Tiger("Tom", 6);
        Archer archer = new Archer("Robin", 7);

        SuperHero superHero = new SuperHero("Iron man",100);

      //  battleArena.startBattle(gladiator, tiger);
       // battleArena.startBattle(archer, tiger);
       // battleArena.startBattle(archer, gladiator);
        battleArena.startBattle(superHero, gladiator);

    }
}
